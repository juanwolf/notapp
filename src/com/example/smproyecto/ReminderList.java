package com.example.smproyecto;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.example.smproyecto.modelo.Reminder;
/**
 * Implement a View of the reminder list.
 * @author jenpoul
 */
public class ReminderList extends ListActivity {



	public final static int ACTIVITY_EDIT = 1;
	// private static final int DELETE_ID = FIRST+1;

	SimpleCursorAdapter adapter;
	NotappSQLiteHelper db;

	public static final int TAG_ADD_REMINDERS = 5;
	public static final String ACTIVITY_TAG_NAME = "tag";
	private Cursor cursor;
	/**
	 * Update the list of notes.
	 */
	private void updateView() {
		Cursor newCursor = db.getReminders();
		adapter.swapCursor(newCursor);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		db = new NotappSQLiteHelper(this);
		cursor = db.getReminders();
		// TODO Change that for the new view.
		adapter = new SimpleCursorAdapter(this,
				R.layout.reminderlist_item, 
				cursor,
				new String[]{
				NotappSQLiteHelper.COLUMN_TITLE,
				NotappSQLiteHelper.COLUMN_DATE,
				NotappSQLiteHelper.COLUMN_REMINDER_DATE,
				NotappSQLiteHelper.COLUMN_TEXT
		},
		new int[] {R.id.title_item_reminderlist,
				R.id.date_item_reminderlist,
				R.id.reminderdate_item_reminderlist,
				R.id.text_item_reminderlist
		},

		android.support.v4.widget.
		CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		setListAdapter(adapter);

		//eliminar item de la lista
		ListView lv = getListView();
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int position,final long id) {

				final int idItem= (int)id;

				// Log.d("noteapp", "entroooo en la mierda esta...");
				Toast.makeText(getApplicationContext(),"posicion: " + position, Toast.LENGTH_SHORT).show();

				// removeListItem(rowView, positon);
				String[] opc = new String[] { "Cancelar", "Eliminar"};                

				AlertDialog opciones = new AlertDialog.Builder(ReminderList.this)
				.setTitle("Opciones")
				.setItems(opc,
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int selected) {

						switch (selected) {
						case 0:
							break;
						case 1:
							//Eliminar
							// Log.d("noteapp", "el id seleccionado es :"+idItem);
							Reminder reminder = new Reminder(idItem,null,null,null,null);
							Log.d("noteapp", "se elimino :"+reminder.getId());
							db.deleteReminder(reminder);
							updateView();
							break;                            	            
						}
					}
				}).create();
				opciones.show();
				return true;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.reminder_list_menu, menu);
		return true;
	}
	// TODO Items menu
	@Override public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.create_reminder: 
			Intent intent = new Intent(this, EditorReminder.class);
			intent.putExtra("tag", TAG_ADD_REMINDERS);
			startActivityForResult(intent, TAG_ADD_REMINDERS);
			break;

		case R.id.refresh_reminder:
			updateView();
			break;

		case R.id.delete_reminder:
			db.deleteAllReminders();
			updateView();
			break;
			
		case R.id.show_notes:
			Intent intentNotes = new Intent(this, NoteList.class);
			startActivity(intentNotes);
			finish();
			break;
		}

		return true;

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);


		cursor = db.getReminders();
		Cursor c = cursor;
		c.moveToPosition(position);

		Intent i = new Intent(this, EditorReminder.class);
		Log.d("noteapp","Esta mierda es lo que esta sucediendo ::"+c.getInt(
				c.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_ID)));
		i.putExtra(NotappSQLiteHelper.COLUMN_ID, c.getInt(
				c.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_ID)));
		i.putExtra(NotappSQLiteHelper.COLUMN_TITLE, c.getString(
				c.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_TITLE)));
		i.putExtra(NotappSQLiteHelper.COLUMN_TEXT, c.getString(
				c.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_TEXT)));
		i.putExtra(NotappSQLiteHelper.COLUMN_REMINDER_DATE, c.getString(
				c.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_REMINDER_DATE)));
		
		i.putExtra("tag", ACTIVITY_EDIT);
		startActivityForResult(i, ACTIVITY_EDIT);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		switch (requestCode) {
		case TAG_ADD_REMINDERS :
			updateView();
			Log.d("noteapp", "Entra en el resultado Agregar Reminder");
			break;
		case ACTIVITY_EDIT : 
			updateView();
			Log.d("noteapp", "Entra en el resultado Editar Reminder");
			break;
		}
	}
}


