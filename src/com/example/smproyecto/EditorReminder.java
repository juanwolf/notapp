package com.example.smproyecto;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.smproyecto.modelo.Reminder;

public class EditorReminder extends FragmentActivity {
	private EditText titleReminder;
	private EditText textReminder;
	private static String dayReminder;
	private static String timeReminder;
	
	static Button dayPicker; 
	static Button timePicker;
	
	public int tagActivity;
	private int rowId;
	
	public static class DatePickerFragment extends DialogFragment
	implements DatePickerDialog.OnDateSetListener {
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			month++;
			if (month < 10) {
				dayReminder = year + "-" + "0" + month + "-" + day;
			} else {
				dayReminder = year + "-" + month + "-" + day;
			}
			dayPicker.setText(dayReminder);
		}
	}


	public static class TimePickerFragment extends DialogFragment
	implements TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			if (hourOfDay < 10) {
				timeReminder = "0" + hourOfDay + ":";
			} else {
				timeReminder = hourOfDay + ":";	
			}
			if (minute < 10) {
				timeReminder += "0" + minute;
			} else {
				timeReminder += minute;
			}
			timePicker.setText(timeReminder);
		}
	}

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editor_reminder);
		timeReminder = null;
		dayReminder = null;
		titleReminder = 
				(EditText) findViewById(R.id.edit_title_reminder);
		textReminder = 
				(EditText) findViewById(R.id.edit_text_reminder);
		dayPicker = 
				(Button) findViewById(R.id.button_day_picker);
		timePicker = 
				(Button) findViewById(R.id.button_time_picker);
		Button buttonAccept = 
				(Button) findViewById(R.id.button_accept_reminder);
		Button buttonCancel =
				(Button) findViewById(R.id.button_cancel_reminder);

		Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
			tagActivity = extras.getInt(ReminderList.ACTIVITY_TAG_NAME);
			if (tagActivity == ReminderList.ACTIVITY_EDIT) {
				rowId = extras.getInt(NotappSQLiteHelper.COLUMN_ID);
				String title = extras.getString(NotappSQLiteHelper.COLUMN_TITLE);
				String body = extras.getString(NotappSQLiteHelper.COLUMN_TEXT);
				String dateReceived = extras.getString(NotappSQLiteHelper.COLUMN_REMINDER_DATE);
				String[] splitReceived = dateReceived.split(" ");
				String timeReceived = splitReceived[1];
				String daysReceived = splitReceived[0];
				
				titleReminder.setText(title);
				textReminder.setText(body);
				dayPicker.setText(daysReceived);
				timePicker.setText(timeReceived);
			}
		}
		
		buttonCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlertDialog.Builder opciones = new AlertDialog.Builder(
						EditorReminder.this);
				opciones.setTitle(getResources()
						.getString(R.string.btnCancelar));
				opciones.setMessage(getResources().getString(
						R.string.MensajeCancelar));
				opciones.setCancelable(false);
				opciones.setPositiveButton(
						getResources().getString(R.string.btnAceptar),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialogo1, int id) {
								onBackPressed();
							}
						});
				opciones.setNegativeButton(
						getResources().getString(R.string.btnCancelar),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialogo1, int id) {

							}
						});
				opciones.show();
				
			}
		});
		
		// Boton para definir un dia.
		dayPicker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				DatePickerFragment tmpDayPicker = new DatePickerFragment();
				tmpDayPicker.show(getSupportFragmentManager(), "dayPicker");
			}
		});
		
		// Boton para definir una hora.
		timePicker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TimePickerFragment tmpTimePicker = 
						new TimePickerFragment();
				tmpTimePicker.show(getSupportFragmentManager(), "timePicker");
			}
		});
		
		// Alert Dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Warning : The dates are null !")
				.setTitle("Error")
		        .setCancelable(false)
		        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		        	public void onClick(DialogInterface dialog, int id) {
		                //do things
		        	}
		        });
		final AlertDialog alert = builder.create();
		buttonAccept.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String title = titleReminder.getText().toString();
				SimpleDateFormat dateFormater = 
						new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
				Date date = new Date();
				String creationDate = dateFormater.format(date);
				String description = textReminder.getText().toString();
				if (dayReminder != null && timeReminder != null) {
					NotappSQLiteHelper db = 
							new NotappSQLiteHelper(getApplicationContext());
					String alarmDate = dayReminder + " " + timeReminder;
					if (tagActivity == NoteList.TAG_ADD_NOTES) {
						Reminder reminder = new Reminder(title, creationDate,
								alarmDate, description);
						db.addReminder(reminder);
					} else if (tagActivity == NoteList.ACTIVITY_EDIT) {
						Reminder reminder = new Reminder(rowId, title,
								creationDate, alarmDate, description);
						db.updateReminder(reminder);
					}		
					finish();
				} else {
					alert.show();
				}
			}
		});
				
	}
}
