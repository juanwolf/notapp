package com.example.smproyecto;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notelist_item);
        /*// For testing the Database, uncomment this part.*/
       // NotappSQLiteHelper db = new NotappSQLiteHelper(this);
       // db.testClass();
        
    }
    private void cargarEditorNota(){
   	 Intent intent = new Intent(this, EditorNota.class);
   	 startActivity(intent);
   }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
          
        case R.id.crearNota: 

           cargarEditorNota();
           break;
            
        }

        return true;

 }
}
