package com.example.smproyecto;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.smproyecto.modelo.Note;

public class EditorNota extends Activity {
	// atributos para manejar directorio externo
	BroadcastReceiver mExternalStorageReceiver;
	boolean mExternalStorageAvailable = false;
	boolean mExternalStorageWriteable = false;

	/* Atributos globales */
	private EditText tituloNota;
	private EditText infoNota;
	private int mRowId;
	private int tagActivity;

	// variables uso de camara
	Intent intentCamara;
	int cameraData = 0;
	String name = "";
	String nameNew = "";
	BitmapFactory.Options options;

	// variable para capturar el nombre asignado a la imagen
	public String nombreImagen = "";
	public int datosRandomIMG;

	private static int CAMERA_PICTURE = 1;
	private static int GALLERY_PICTURE = 2;
	int code = CAMERA_PICTURE;

	ImageView imageView = null;
	String imagen = "";
	String picture = "";
	private Rect outPadding;

	// rotacion de las imagenes
	int rotate = 0;
	Bitmap mySecondBitmap = null;
	BitmapFactory.Options optionsRotate = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editornota);

		tituloNota = (EditText) findViewById(R.id.tituloNota);
		infoNota = (EditText) findViewById(R.id.infoNota);

		mRowId = 0;

		imageView = (ImageView) findViewById(R.id.imageViewNota);
		Button botonAceptar = (Button) findViewById(R.id.btnGuardarNota);
		Button botonCancelar = (Button) findViewById(R.id.btnCancelar);
		Button botonCamara = (Button) findViewById(R.id.btnCamara);

		// Se verifica si se estan recibiendo datos de un intent
		// para luego mostrarlos en el editor de texto
		Bundle extras = getIntent().getExtras();

		if (extras != null) {

			Matrix mtrx = new Matrix();

			String title = extras.getString(NotappSQLiteHelper.COLUMN_TITLE);
			String body = extras.getString(NotappSQLiteHelper.COLUMN_TEXT);
			imagen = extras.getString(NotappSQLiteHelper.COLUMN_PICTURE);
			tagActivity = extras.getInt(NoteList.ACTIVITY_TAG_NAME);
			mRowId = extras.getInt(NotappSQLiteHelper.COLUMN_ID);

			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 8;

			// detectar la orientacion de la imagen
			if (imagen != null) {

				try {
					ExifInterface exifP = new ExifInterface(imagen);
					int orientation = exifP.getAttributeInt(
							ExifInterface.TAG_ORIENTATION,
							ExifInterface.ORIENTATION_NORMAL);

					switch (orientation) {
					case ExifInterface.ORIENTATION_ROTATE_270:
						rotate = 270;
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						rotate = 180;
						break;
					case ExifInterface.ORIENTATION_ROTATE_90:
						rotate = 90;
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				mtrx.postRotate(rotate);

				Bitmap myBitmap = BitmapFactory.decodeFile(imagen, options);
				mySecondBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
						myBitmap.getWidth(), myBitmap.getHeight(), mtrx, true);

				imageView.setImageBitmap(mySecondBitmap);
			}

			if (title != null) {
				tituloNota.setText(title);
			}
			if (body != null) {
				infoNota.setText(body);
			}

		}

		// boton para guardar nota en la base de datos

		botonAceptar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				// se guarda la nota en caso de que no exista
				String title = tituloNota.getText().toString();
				SimpleDateFormat dateFormater = new SimpleDateFormat(
						"yyyy-MM-dd", Locale.ENGLISH);

				Date date = new Date();
				String dateString = dateFormater.format(date);
				String text = infoNota.getText().toString();
				String category = "Nada";

				if (name != "") {
					picture = name;
				} else {
					picture = imagen;
				}

				NotappSQLiteHelper db = new NotappSQLiteHelper(
						getApplicationContext());

				if (NoteList.TAG_ADD_NOTES == tagActivity) {
					Log.d("noteapp", "Start CREATE Notes");
					Note note = new Note(title, dateString, text, category,
							picture);
					db.addNote(note);
				} else if (NoteList.ACTIVITY_EDIT == tagActivity) {
					Log.d("noteapp", "Start Edition Notes");
					Note note = new Note(mRowId, title, dateString, text,
							category, picture);
					db.updateNote(note);
				}
				finish();

			}

		});

		// boton para tomar foto
		botonCamara.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				String[] opc = new String[] {
						getResources().getString(R.string.btnCamara),
						getResources().getString(R.string.btnGaleria) };

				AlertDialog opciones = new AlertDialog.Builder(EditorNota.this)
						.setTitle(getResources().getString(R.string.CabOption))
						.setItems(opc, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int selected) {

								switch (selected) {
								case 0:
									// camara

									String nombreImg = generarNombre();

									File RutaFolder = crearFolder();

									name = RutaFolder.getAbsolutePath() + "/"
											+ nombreImg + ".jpg";

									// creo el intent que permitirá trabajar con
									// la camara del dispositivo
									intentCamara = new Intent(
											MediaStore.ACTION_IMAGE_CAPTURE);
									Uri output = Uri.fromFile(new File(name));
									code = CAMERA_PICTURE;
									intentCamara.putExtra(
											MediaStore.EXTRA_OUTPUT, output);
									break;
								case 1:
									// galeria
									// creo el intent de la galeria
									intentCamara = new Intent(
											Intent.ACTION_PICK,
											android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
									code = GALLERY_PICTURE;
									break;
								}
								// lanzo la actividad de la camara o de la
								// Galeria
								startActivityForResult(intentCamara, code);
							}
						}).create();
				opciones.show();

			}
		});

		// boton para cancelar la accion de crear o editar la nota
		botonCancelar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder opciones = new AlertDialog.Builder(
						EditorNota.this);
				opciones.setTitle(getResources()
						.getString(R.string.btnCancelar));
				opciones.setMessage(getResources().getString(
						R.string.MensajeCancelar));
				opciones.setCancelable(false);
				opciones.setPositiveButton(
						getResources().getString(R.string.btnAceptar),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialogo1, int id) {
								onBackPressed();
							}
						});
				opciones.setNegativeButton(
						getResources().getString(R.string.btnCancelar),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialogo1, int id) {

							}
						});
				opciones.show();
			}
		});

		imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if (imagen != null || name != "") {

					Intent intentView = new Intent(EditorNota.this,
							view_image_note.class);

					if (imagen != "" && name == "") {
						intentView.putExtra("rutaImage", imagen);
					} else {
						intentView.putExtra("rutaImage", name);
					}
					startActivity(intentView);

				}

			}
		});
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(name!=""){
			outState.putString("CONT", name);
		}else{
			outState.putString("CONT", imagen);
		}
		Log.d("noteapp", "entra a salvar"+name+" img_"+imagen);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		name = savedInstanceState.getString("CONT");
        imagen= savedInstanceState.getString("CONT");
        
        Log.d("noteapp", "entra a salvar"+savedInstanceState.getString("CONT"));
        
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;
		File imgF = new File(savedInstanceState.getString("CONT"));
		Bitmap myBitmap = BitmapFactory.decodeFile(imgF.getAbsolutePath(),
				options);
		imageView.setImageBitmap(myBitmap);

	}

	// METODO PARA MOSTRAR LA IMAGEN CAPTURADA
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;

		// capturo la ruta absoluta del archivo
		File imgFile = new File(name);
		Log.d("noteapp", "Entra en ONACTIVITYRESULT");

		// detectar la orientacion de la imagen
		try {
			ExifInterface exif = new ExifInterface(imgFile.getAbsolutePath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Matrix matrix = new Matrix();
		matrix.postRotate(rotate);

		if (requestCode == CAMERA_PICTURE) {

			Bitmap myBitmap = BitmapFactory.decodeFile(
					imgFile.getAbsolutePath(), options);
			mySecondBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
					myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);

			imageView.setImageBitmap(myBitmap);

		} else if (requestCode == GALLERY_PICTURE) {

			Uri selectedImage = data.getData();
			String imageFile = getRealPathFromURI(selectedImage);
			InputStream is;
			name = imageFile;

			try {
				is = getContentResolver().openInputStream(selectedImage);
				BufferedInputStream bis = new BufferedInputStream(is);
				Bitmap bitmap = BitmapFactory.decodeStream(bis, outPadding,
						options);

				mySecondBitmap = Bitmap.createBitmap(bitmap, 0, 0,
						bitmap.getWidth(), bitmap.getHeight(), matrix, true);

				imageView.setImageBitmap(bitmap);
			} catch (FileNotFoundException e) {
			}

		}
	}

	private String getRealPathFromURI(Uri contentURI) {
		Cursor cursor = getContentResolver().query(contentURI, null, null,
				null, null);
		cursor.moveToFirst();
		int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
		return cursor.getString(idx);
	}

	public String generarNombre() {
		// defino el nombre de la imagen a guardar
		Random generadorRandom = new Random();
		for (int a = 1; a <= 10000; ++a) {
			datosRandomIMG = generadorRandom.nextInt(10000);
		}
		return nombreImagen = "NAIMG" + datosRandomIMG;

	}

	public File crearFolder() {
		File rutaFolder = null;
		// verifico si existe permiso en el dispositivo
		boolean write = isExternalStorageWritable();
		boolean read = isExternalStorageReadable();

		if (write == true && read == true) {

			// creo la carpeta dentro de la SD
			File folder = new File(Environment.getExternalStorageDirectory()
					+ "/NoteApp");
			folder.mkdirs();

			// verifico si se creo correctamente la carpeta
			boolean success = true;
			if (!folder.exists()) {
				success = folder.mkdir();

			}
			if (success) {
				// Do something on success
				success = folder.mkdir();
				rutaFolder = folder;

			} else {
				// Do something else on failure

			}
		} else {

			ContextWrapper cw = new ContextWrapper(getApplicationContext());
			// path to /data/data/yourapp/app_data/imageDir
			File folderInterno = cw.getDir("noteapp", Context.MODE_PRIVATE);

			rutaFolder = folderInterno;
		}
		return rutaFolder;
	}

	/* Checks if external storage is available for read and write */
	public boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}

	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)
				|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}

}
