package com.example.smproyecto;

import android.app.AlertDialog;
import android.app.ListActivity;
import com.example.smproyecto.EditorNota;
import com.example.smproyecto.NotappSQLiteHelper;
import com.example.smproyecto.modelo.Note;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Implement a View of the note list at the launching of the application.
 * 
 * @author jenpoul
 */
public class NoteList extends ListActivity {

	public final static int ACTIVITY_EDIT = 1;

	SimpleCursorAdapter adapter;
	NotappSQLiteHelper db;

	public static final int TAG_ADD_NOTES = 5;
	public static final String ACTIVITY_TAG_NAME = "tag";
	private Cursor cursor;

	/**
	 * Update the list of notes.
	 */
	private void updateView() {
		Cursor newCursor = db.getNotes();
		adapter.swapCursor(newCursor);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		db = new NotappSQLiteHelper(this);
		// Run this function the first time for adding something to db
		// db.testClass();
		cursor = db.getNotes();
		adapter = new SimpleCursorAdapter(
				this,
				R.layout.notelist_item,
				cursor,
				new String[] { NotappSQLiteHelper.COLUMN_TITLE,
						NotappSQLiteHelper.COLUMN_DATE,
						NotappSQLiteHelper.COLUMN_TEXT },
				new int[] { R.id.title_item_list, R.id.date_item_list,
						R.id.text_item_list },

				android.support.v4.widget.CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		setListAdapter(adapter);

		// eliminar item de la lista
		ListView lv = getListView();
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int position, final long id) {

				final int idItem = (int) id;

				Toast.makeText(getApplicationContext(),
						"posicion: " + position, Toast.LENGTH_SHORT).show();

				String[] opc = new String[] { "Cancelar", "Eliminar" };

				AlertDialog opciones = new AlertDialog.Builder(NoteList.this)
						.setTitle("Opciones")
						.setItems(opc, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int selected) {

								switch (selected) {
								case 0:
									break;
								case 1:
									Note note = new Note(idItem, null, null,
											null, null, null);
									Log.d("noteapp",
											"se elimino :" + note.getId());
									db.deleteNote(note);
									updateView();
									break;
								}
							}
						}).create();
				opciones.show();
				return true;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.crearNota:
			Intent intent = new Intent(this, EditorNota.class);
			intent.putExtra("tag", TAG_ADD_NOTES);
			startActivityForResult(intent, TAG_ADD_NOTES);
			break;

		case R.id.refresh_notes:
			updateView();
			break;

		case R.id.delete_notes:
			db.deleteAllNotes();
			updateView();
			break;

		case R.id.show_reminders:
			Intent intentReminder = new Intent(this, ReminderList.class);
			startActivity(intentReminder);
			finish();
			break;
		}

		return true;

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		cursor = db.getNotes();
		Cursor c = cursor;
		c.moveToPosition(position);

		Intent i = new Intent(this, EditorNota.class);

		i.putExtra(NotappSQLiteHelper.COLUMN_ID,
				c.getInt(c.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_ID)));
		i.putExtra(NotappSQLiteHelper.COLUMN_TITLE, c.getString(c
				.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_TITLE)));
		i.putExtra(NotappSQLiteHelper.COLUMN_TEXT, c.getString(c
				.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_TEXT)));
		i.putExtra(NotappSQLiteHelper.COLUMN_PICTURE, c.getString(c
				.getColumnIndexOrThrow(NotappSQLiteHelper.COLUMN_PICTURE)));
		i.putExtra("tag", ACTIVITY_EDIT);
		startActivityForResult(i, ACTIVITY_EDIT);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		switch (requestCode) {
		case TAG_ADD_NOTES:
			updateView();
			break;
		case ACTIVITY_EDIT:
			updateView();
			break;
		}
	}
}
