package com.example.smproyecto;

import java.io.File;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

public class view_image_note extends Activity {

	int rotate = 0;
	Bitmap myBitmap=null;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_image_note);

		
		ImageView imageViewNote = (ImageView) findViewById(R.id.imageViewFull);

		Bundle bundle = getIntent().getExtras();

		String name = bundle.getString("rutaImage");
		
		Log.d("noteapp", "Entra en la pantalla imageViewNote -->"+name);
		File imgF = new File(name);
		BitmapFactory.Options options = null;
		
		//detectar la orientacion de la imagen
		try {
		ExifInterface exif = new ExifInterface(imgF.getAbsolutePath());
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
        case ExifInterface.ORIENTATION_ROTATE_270:
            rotate = 270;
            break;
        case ExifInterface.ORIENTATION_ROTATE_180:
            rotate = 180;
            break;
        case ExifInterface.ORIENTATION_ROTATE_90:
            rotate = 90;
            break;
        }
		}catch (Exception e) {
	        e.printStackTrace();
	    }
		
		Matrix matrix = new Matrix();
        matrix.postRotate(rotate);

		
		int tamanoImage=(int) imgF.length();
		Log.d("noteapp","el tamaño de la foto es-->"+tamanoImage);
		
		if(imgF.length()<1000000){
			options = new BitmapFactory.Options();
			options.inSampleSize = 1;
		}else if(imgF.length()>=1000000){
			options = new BitmapFactory.Options();
			options.inSampleSize = 8;
		}			
		
		Bitmap myFirstBitmap = BitmapFactory.decodeFile(imgF.getAbsolutePath(),
				options);
		myBitmap = Bitmap.createBitmap(myFirstBitmap , 0, 0, myFirstBitmap.getWidth(), myFirstBitmap.getHeight(), matrix, true);
		imageViewNote.setImageBitmap(myBitmap);
	}
}
