package com.example.smproyecto.modelo;

/**
 * Clase por los objetos de Reminder.
 *
 */
public class Reminder {
	private int id;
	private String title;
	private String creationDate;
	private String alarmDate;
	private String description;
	
	
	public Reminder() {
		title = null;
		creationDate = null;
		alarmDate = null;
	}
	
	public Reminder(int id, String title, String creationDate, 
			String alarmDate, String description) {
		this.id = id;
		this.title = title;
		this.creationDate = creationDate;
		this.alarmDate = alarmDate;
		this.description = description;
	}
	
	public Reminder(String title, String creationDate,
					String alarmDate, String description) {
		this.title = title;
		this.creationDate = creationDate;
		this.alarmDate = alarmDate;
		this.description = description;
	}
	
	// Getters -----------------------------------------------------------------
	
	public int getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getAlarmDate() {
		return alarmDate;
	}

	public void setAlarmDate(String alarmDate) {
		this.alarmDate = alarmDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
