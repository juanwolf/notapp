package com.example.smproyecto.modelo;


/**
 * Model which represent Notes.
 * inv:
 *     id != 0
 * 
 *     for a, b : Note
 * 		   a.id != b.id
 *     title != null
 *     date = now;
 * 
 * @author jenpoul
 *
 */
public class Note {
	private static final String REGEXP_DATE = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
	private int id;
	private String title;
	private String date;
	private String text;
	private String category;
	private String picture;
	
	public Note() {
		// Empty Constructor.
	}
	
	public Note(int id, String title, String date, String text,
			String category, String picture) {
		this.id = id;
		this.title = title;
		this.date = date;
		this.text = text;
		this.category = category;
		this.picture = picture;
	}
	
	public Note(String title, String date, String text,
			String category, String picture) {
		this.title = title;
		this.date = date;
		this.text = text;
		this.category = category;
		this.picture = picture;
	}
	
	
	// Getters ----------------------------------------
	public int getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getText() {
		return text;
	}
	
	public String getCategory() {
		return category;
	}
	
	public String getPicture() {
		return picture;
	}
	
	//Setters -----------------------------------------
	public void setId(int newId) {
		id = newId;
	}
	
	public void setTitle(String newTitle) {
		if (newTitle == null) {
			throw new IllegalArgumentException("Title is null");
		}
		title = newTitle;
	}
	
	public void setDate(String newDate) {
		if (!(newDate.matches(REGEXP_DATE))) {
			throw new IllegalArgumentException("Bad format for date");
		}
		date = newDate;
	}
	
	public void setText(String newText) {
		if (newText == null) {
			throw new IllegalArgumentException("Text is null");
		}
		text = newText;
	}
	
	public void setCategory(String newCategory) {
		if (newCategory == null) {
			throw new IllegalArgumentException("Null Category");
		}
		category = newCategory;
	}
	
	public void setPicture(String newPicture) {
		//if (newPicture == null) {
		//	throw new IllegalArgumentException();
		//}
	}
	
}
