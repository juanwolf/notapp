package com.example.smproyecto;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.smproyecto.modelo.Note;
import com.example.smproyecto.modelo.Reminder;

/**
 * Class which be the connection between  Notapp and the database.
 * 
 * @author jenpoul
 */
public class NotappSQLiteHelper extends SQLiteOpenHelper {

	private static SQLiteDatabase db;

	// Datas about the database
	private static final String DATABASE_NAME = "notapp.db";
	private static final int DATABASE_VERSION = 2;

	// All datas about the table notes
	public static final String TABLE_NOTES = "notes";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_DATE = "date";
	public static final String COLUMN_TEXT = "textNote";
	public static final String COLUMN_CATEGORY = "category";
	public static final String COLUMN_PICTURE = "picture";


	// All datas about the table reminder
	public static final String TABLE_REMINDER = "reminder";
	public static final String COLUMN_REMINDER_DATE ="reminder_date";

	// Database creation sql statement
	private static final String DATABASE_CREATE_TABLE_NOTE = "create table " 
			+ TABLE_NOTES + "(" 
			+ COLUMN_ID + " integer primary key autoincrement, " 
			+ COLUMN_TITLE + " text not null, "
			+ COLUMN_DATE + " text not null, " // Format "YYYY-MM-DD HH:MM:SS"
			+ COLUMN_TEXT + " text, "
			+ COLUMN_CATEGORY + " text, "
			+ COLUMN_PICTURE + " text " // Link for the picture
			+ "); ";

	// Creation query of the table Reminder 
	private static final String DATABASE_CREATE_TABLE_REMINDER = " create table "
			+ TABLE_REMINDER + "("
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_TITLE + " text not null, "
			+ COLUMN_DATE + " text not null, " // Format "YYYY-MM-DD HH:MM:SS"
			+ COLUMN_REMINDER_DATE + " text, " // Format "YYYY-MM-DD HH:MM:SS"
			+ COLUMN_TEXT + " text "
			+ ");";			

	/**
	 * Constructor
	 * @param context
	 */
	public NotappSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE_TABLE_NOTE);
		database.execSQL(DATABASE_CREATE_TABLE_REMINDER);
		Cursor c = database.rawQuery("SELECT * FROM " + TABLE_REMINDER, null);
		Log.d("notapp", "nb column :" + c.getColumnCount());
		Log.d("notapp", "db toString : " + c.toString());
		for (int i = 0; i < c.getColumnCount(); i++) {
			Log.d("notapp", c.getColumnName(i));
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES + ", " 
				+ TABLE_REMINDER);
		onCreate(db);
	}

	// Notes Section -----------------------------------------------------------
	/**
	 * Add a note in the database.
	 * @param note != null
	 */
	public void addNote(Note note) {
		if (note == null) {
			throw new IllegalArgumentException();
		}
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(COLUMN_TITLE, note.getTitle());
		values.put(COLUMN_DATE, note.getDate());
		values.put(COLUMN_TEXT, note.getText());
		values.put(COLUMN_CATEGORY, note.getCategory());
		values.put(COLUMN_PICTURE, note.getPicture());

		// Inserting Row
		db.insert(TABLE_NOTES, null, values);
		Log.d("noteapp", "Adding \n" 
				+ " id : " + note.getId()
				+ " Title : " + note.getTitle()
				+ " date : " + note.getDate()
				+ " text : " + note.getText()
				+ " category : " + note.getCategory()
				+ " picture : " + note.getPicture());
		db.close(); // Closing database connection
	}

	/**
	 * Return the note with id = id
	 * @param id >= 0
	 * @return Note.id == id
	 */
	public Note getNote(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_NOTES, new String[]{
				COLUMN_TITLE, COLUMN_DATE, COLUMN_TEXT,
				COLUMN_CATEGORY, COLUMN_PICTURE},
				COLUMN_ID + "=?",
				new String[] {String.valueOf(id)},
				null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Note note = new Note(Integer.parseInt(cursor.getString(0)), 
				cursor.getString(1), cursor.getString(2), 
				cursor.getString(3), cursor.getString(4),
				cursor.getString(5));
		return note;
	}

	/**
	 * Return the list of all the note in the database.
	 * @return 
	 */
	public List<Note> getAllNote() {
		List<Note> noteList = new ArrayList<Note>();
		String selectQuery = "SELECT * FROM " + TABLE_NOTES;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Note note = new Note();
				note.setId(Integer.parseInt(cursor.getString(0)));
				note.setTitle(cursor.getString(1));
				note.setDate(cursor.getString(2));
				note.setText(cursor.getString(3));
				note.setCategory(cursor.getString(4));
				note.setPicture(cursor.getString(5));

				// Add note to list
				noteList.add(note);
			} while(cursor.moveToNext());
		}
		return noteList;
	}

	/**
	 * Function used for bind the database to a view.
	 * @return Cursor with all notes.
	 */
	public Cursor getNotes() {
		String selectQuery = "SELECT * FROM " + TABLE_NOTES;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		return cursor;
	}


	public int updateNote(Note note) {
		db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(COLUMN_TITLE, note.getTitle());
		values.put(COLUMN_DATE, note.getDate());
		values.put(COLUMN_TEXT, note.getText());
		values.put(COLUMN_CATEGORY, note.getCategory());
		values.put(COLUMN_PICTURE, note.getPicture());

		Log.d("noteapp", " updating : " 
				+ "id    : " + note.getId()
				+ "title : " + note.getTitle()
				+ " date : " + note.getDate()
				+ " text : " + note.getText());
		//updating row
		return db.update(TABLE_NOTES, values, COLUMN_ID + " = ?",
				new String[] { String.valueOf(note.getId())});				
	}

	public void deleteNote(Note note) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_NOTES, COLUMN_ID + " = ?",
				new String[]{String.valueOf(note.getId())});
		//notifyAll(); 
		db.close();
		Log.d("noteapp", "item eliminado en la clase :"+note.getId());
		//new String[]{String.valueOf(note)});
	}

	public void deleteAllNotes() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_NOTES);
		db.close();
	}

	public int getNoteCount() {
		String countQuery = "SELECT  * FROM " + TABLE_NOTES;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

	public void testClass() {
		// Inserting Contact
		Log.d("noteapp", "Inserting...");
		this.addNote(new Note("Why note ? (HAHA)",
				"2012-09-26", "Vini vidi vici",
				"Useless", null));
		this.addNote(new Note("Yolo !", 
				"2013-10-20", "Ok all is allright, all work, i think",
				"Empty", null));

		Log.d("noteapp", "Reading all notes...");
		List<Note> notes = this.getAllNote();

		for (Note n : notes) {
			String log = "Id: " + n.getId() 
					+ ", Title: " + n.getTitle()
					+ " , Date : " + n.getDate()
					+ " , Text : " + n.getText()
					+ " , Category : " + n.getCategory();
			Log.d("noteapp", log);
		}

	}
	
	public boolean updateNote(String title, String body) {
		ContentValues args = new ContentValues();
		args.put(COLUMN_TITLE, title);
		args.put(COLUMN_TEXT, body);

		return db.update(TABLE_NOTES, args, null ,null) > 0;
	}
	
	// Reminder section --------------------------------------------------------
	
	public Reminder getReminder(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_REMINDER, new String[]{
				COLUMN_TITLE, COLUMN_DATE, COLUMN_TEXT,
				COLUMN_CATEGORY},
				COLUMN_ID + "=?",
				new String[] {String.valueOf(id)},
				null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Reminder reminder = new Reminder(Integer.parseInt(cursor.getString(0)), 
				cursor.getString(1), cursor.getString(2), 
				cursor.getString(3), cursor.getString(4));
		return reminder;
	}
	
	/**
	 * Function used for bind the database to a view.
	 * @return Cursor with all reminders.
	 */
	public Cursor getReminders() {
		String selectQuery = "SELECT * FROM " + TABLE_REMINDER;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		return cursor;
	}
	
	public int updateReminder(Reminder reminder) {
		db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(COLUMN_TITLE, reminder.getTitle());
		values.put(COLUMN_DATE, reminder.getCreationDate());
		values.put(COLUMN_REMINDER_DATE, reminder.getAlarmDate());
		values.put(COLUMN_TEXT, reminder.getDescription());

		Log.d("noteapp", " updating Reminder : " 
				+ "id    : " + reminder.getId()
				+ "title : " + reminder.getTitle()
				+ " date : " + reminder.getCreationDate()
				+ " alarm date : " + reminder.getAlarmDate() 
				+ " text : " + reminder.getDescription());
		//updating row
		return db.update(TABLE_REMINDER, values, COLUMN_ID + " = ?",
				new String[] { String.valueOf(reminder.getId())});				
	}
	
	public void addReminder(Reminder reminder) {
		if (reminder == null) {
			throw new IllegalArgumentException();
		}
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(COLUMN_TITLE, reminder.getTitle());
		values.put(COLUMN_DATE, reminder.getCreationDate());
		values.put(COLUMN_TEXT, reminder.getDescription());
		values.put(COLUMN_REMINDER_DATE, reminder.getAlarmDate());

		// Inserting Row
		db.insert(TABLE_REMINDER, null, values);
		Log.d("noteapp", "Adding \n" 
				+ " id : " + reminder.getId()
				+ " Title : " + reminder.getTitle()
				+ " date : " + reminder.getCreationDate()
				+ " Description : " + reminder.getDescription()
				+ " Alarm : " + reminder.getAlarmDate());
		db.close(); // Closing database connection
	}
	
	public void deleteReminder(Reminder reminder) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_REMINDER, COLUMN_ID + " = ?",
				new String[]{String.valueOf(reminder.getId())}); 
		db.close();
		Log.d("noteapp", "item eliminado en la clase :" + reminder.getId());
	}
	
	public void deleteAllReminders() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_REMINDER);
		db.close();
	}

	public int getReminderCount() {
		String countQuery = "SELECT  * FROM " + TABLE_REMINDER;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

}
